<?php

class Cookie {

    public function create($name, $value) {
         setcookie($name, $value);
    }

    public function delete($name) {
          setcookie($name, '', time()-60*60*24);
          unset($_COOKIE[$name]);
    }

    public function update($name, $value) {
        setcookie($name, $value);
    }

    public function read($name) {
           return isset($_COOKIE[$name]) ? $_COOKIE[$name] : null;
    }

}